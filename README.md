# VEO-Nutthapol

## Disclaimer
This project is not setup with SST because of the extra steps of setting up credentials and AWS account in the limited time of the project.

Further more it can take some time to spin up the serverless functions and the database when using SST. This is not ideal for a project that is supposed to be a demo and have a 4 hours limit.

## Prerequisites
- NodeJS installed on the machine running this project
- Docker installed on the machine running this project
- Git installed and configured on the machine running this project

## Setup Server
1. Navigate into the server folder.

2. Run `npm install` from the root of the server-folder in the terminal to install dependencies

3. Run `docker-compose up -d` from the root of the server-folder in the terminal to start the database

4. Create a `.env` file in the root of the server-folder and add the following variables:
```
DATABASE_URL="postgresql://postgres:postgres@localhost:5432/veo"
PORT=3000
```
The variables should reflect the values in the `docker-compose.yml` file while the PORT is the port you want the express server to run on.

5. Run `npx prisma migrate dev` or `npx prisma db push` from the root of the server-folder in the terminal to create the database with the schema

6. Run `npx prisma generate` from the root of the server-folder in the terminal to generate the prisma client

## Development Server

To start the development server, run the following command from the root of the server folder in your terminal:

```sh
npm run dev
```

To view the project, navigate to the provided url in your terminal which should default to: `http://localhost:3000` 

If you have changed the port, navigate to that port instead.

NOTE: if you want an overview of your database you can run `npx prisma studio` from the root of the server-folder in the terminal to open prisma studio in your browser.

## Test Server
To run the tests for the api-server, run the following command from the root of the service-folder in your terminal:

```sh
npm run test
```

## Setup Client (created with `npx create-remix@latest`)
1. Navigate into the client folder.
2. Run `npm install` from the root of the client-folder in the terminal to install dependencies
3. In `baseConnection.ts` change the `baseURL` to the url of the server you want to connect to. This should be the url of the server you started in the previous section.

## Development Client
1. Navigate into the client folder.
2. Run `npm run dev` from the root of the client-folder in the terminal to start the development server
3. To view the project, navigate to the provided url in your terminal which should default to: `http://localhost:4000`

## Notes & major helping points:
- [Basis for the Node model](https://www.prisma.io/docs/concepts/components/prisma-schema/relations/self-relations#one-to-many-self-relations) 
- [ZOD](https://zod.dev/?id=schema-methods)

- If the cache from react query is playing games try to hard reload the page with `ctrl + shift + r` or `cmd + shift + r` depending on your OS.