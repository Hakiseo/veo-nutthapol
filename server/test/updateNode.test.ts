import { afterAll, beforeAll, expect, test } from "vitest";
import { CreateNodeCommand, createNode } from "../src/mutations/createNode";
import { updateParentOfNode } from "../src/mutations/updateNode";
import { db } from "../src/dbConnection";

let createdCeoId: number;

//Running tests currently deletes all data in the database
beforeAll(async () => {
    await db.node.deleteMany();

    const createdCeo = await db.node.create({
        data: {
            name: "CEO",
            height: 0,
            nodeType: "ceo",
        }
    })
    createdCeoId = createdCeo.id;
})

afterAll(async () => {
    await db.node.deleteMany();
})

test("Should be able to give a node a new parent", async () => {
    //ARRANGE
    const managerInput = {
        name: "manager to update on",
        parentId: createdCeoId,
        nodeType: "manager",
        department: "IT",
    } satisfies CreateNodeCommand
    const createdManagerNode = await createNode(managerInput);

    const developerInput1 = {
        name: "Developer 1 to update",
        parentId: createdManagerNode.id,
        nodeType: "developer",
        language: "JS",
    } satisfies CreateNodeCommand

    const createdDeveloper1 = await createNode(developerInput1);

    const developerInput2 = {
        name: "Developer 2 to update",
        parentId: createdDeveloper1.id,
        nodeType: "developer",
        language: "SCALA",
    } satisfies CreateNodeCommand

    const createdDeveloper2 = await createNode(developerInput2);

    console.log("[Changing parent on node test] - Created developer 1: ", createdDeveloper1)
    console.log("[Changing parent on node test] - Created developer 2: ", createdDeveloper2)
    //ACT
    await updateParentOfNode({ nodeId: createdDeveloper1.id, newParentId: createdCeoId });

    const developer1Node = await db.node.findUnique({
        where: { id: createdDeveloper1.id },
        include: { children: true }
    });

    console.log("[Changing parent on node test] - Developer 1 node with children: ", JSON.stringify(developer1Node, null, 2))

    //ASSERT
    expect(developer1Node).toBeDefined();
    expect(developer1Node!.parentId).toBe(createdCeoId);
    expect(developer1Node!.height).toBe(1);
    expect(developer1Node!.children.length).toBe(1);
    expect(developer1Node!.children[0].height).toBe(2);
})

test("Choosing a child of the node as the node's new parent should fail", async () => {
    //ARRANGE
    const managerInput = {
        name: "manager to update on",
        parentId: createdCeoId,
        nodeType: "manager",
        department: "IT",
    } satisfies CreateNodeCommand
    const createdManagerNode = await createNode(managerInput);

    const developerInput1 = {
        name: "Developer 1 to update",
        parentId: createdManagerNode.id,
        nodeType: "developer",
        language: "JS",
    } satisfies CreateNodeCommand

    const createdDeveloper1 = await createNode(developerInput1);

    const developerInput2 = {
        name: "Developer 2 to update",
        parentId: createdDeveloper1.id,
        nodeType: "developer",
        language: "SCALA",
    } satisfies CreateNodeCommand

    const createdDeveloper2 = await createNode(developerInput2);

    //ACT & ASSERT
    expect(updateParentOfNode({
        nodeId: createdManagerNode.id,
        newParentId: createdDeveloper2.id
    }))
        .rejects
        .toThrowError("Parent cannot be a child of the node being moved")
})