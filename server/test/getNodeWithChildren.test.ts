import { beforeAll, afterAll, test, expect } from "vitest";
import { db } from "../src/dbConnection";
import { getNodeWithChildren } from "../src/queries/getChildNodes";
import { CreateNodeCommand, createNode } from "../src/mutations/createNode";

let createdCeoId: number;

//Running tests currently deletes all data in the database
beforeAll(async () => {
    await db.node.deleteMany();

    const createdCeo = await db.node.create({
        data: {
            name: "CEO",
            height: 0,
            nodeType: "ceo",
        }
    })
    createdCeoId = createdCeo.id;
})

afterAll(async () => {
    await db.node.deleteMany();
})

test("Should be able to get the children of a node (1 layer)", async () => {
    //ARRANGE 
    const managerInput = {
        name: "manager",
        parentId: createdCeoId,
        nodeType: "manager",
        department: "IT",
    } satisfies CreateNodeCommand
    const createdManagerNode = await createNode(managerInput);

    const developerInput1 = {
        name: "Developer 1",
        parentId: createdManagerNode.id,
        nodeType: "developer",
        language: "JS",
    } satisfies CreateNodeCommand

    const developerInput2 = {
        name: "Developer 2",
        parentId: createdManagerNode.id,
        nodeType: "developer",
        language: "SCALA",
    } satisfies CreateNodeCommand

    await createNode(developerInput1);
    await createNode(developerInput2);

    //ACT
    const nodeWithChildren = await getNodeWithChildren(createdManagerNode.id);

    console.log("[Get node with one layer of children test] - Manager node with 2 children: ", JSON.stringify(nodeWithChildren, null, 2))

    //ASSERT
    expect(nodeWithChildren.children.length).toBe(2);
    expect(nodeWithChildren.children[0].name).toBe(developerInput1.name);
    expect(nodeWithChildren.children[0].height).toBe(2);
})