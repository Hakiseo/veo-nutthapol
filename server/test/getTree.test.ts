import { afterAll, beforeAll, expect, test } from "vitest";
import { db } from "../src/dbConnection";
import { getTree } from "../src/queries/getTree";
import { CreateNodeCommand, createNode } from "../src/mutations/createNode";

let createdCeoId: number;

//Running tests currently deletes all data in the database
beforeAll(async () => {
    await db.node.deleteMany();

    const createdCeo = await db.node.create({
        data: {
            name: "CEO",
            height: 0,
            nodeType: "ceo",
        }
    })
    createdCeoId = createdCeo.id;
})

afterAll(async () => {
    await db.node.deleteMany();
})

test("Get the tree structure", async () => {
    //ARRANGE 
    const managerInput = {
        name: "manager",
        parentId: createdCeoId,
        nodeType: "manager",
        department: "IT",
    } satisfies CreateNodeCommand
    const createdManagerNode = await createNode(managerInput);

    const developerInput1 = {
        name: "Developer 1",
        parentId: createdManagerNode.id,
        nodeType: "developer",
        language: "JS",
    } satisfies CreateNodeCommand

    const developerInput2 = {
        name: "Developer 2",
        parentId: createdManagerNode.id,
        nodeType: "developer",
        language: "SCALA",
    } satisfies CreateNodeCommand

    const createdDev1 = await createNode(developerInput1);
    await createNode(developerInput2);

    const developerInput3 = {
        name: "Developer 3",
        parentId: createdDev1.id,
        nodeType: "developer",
        language: "JS",
    } satisfies CreateNodeCommand

    const developerInput4 = {
        name: "Developer 4",
        parentId: createdDev1.id,
        nodeType: "developer",
        language: "SCALA",
    } satisfies CreateNodeCommand

    await createNode(developerInput3);
    const createdDev4 = await createNode(developerInput4);

    const developerInput5 = {
        name: "Developer 5",
        parentId: createdDev4.id,
        nodeType: "developer",
        language: "JS",
    } satisfies CreateNodeCommand

    const developerInput6 = {
        name: "Developer 6",
        parentId: createdDev4.id,
        nodeType: "developer",
        language: "SCALA",
    } satisfies CreateNodeCommand

    await createNode(developerInput5);
    await createNode(developerInput6);
    //ACT

    const tree = await getTree();
    console.log("[Get tree structure test] - Tree: ", JSON.stringify(tree, null, 2));

    //ASSERT
    expect(tree.height).toBe(0); //Initial CEO node with children
    expect(tree.children.length).toBe(1); //1 manager node
    expect(tree.children[0].children.length).toBe(2); //2 developer nodes under manager
    expect(tree.children[0].children[0].parentId).toBe(tree.children[0].id); //developer 1 has manager as parent
    expect(tree.children[0].children[0].children.length).toBe(2); //2 developer nodes under developer 1
    expect(tree.children[0].children[0].children[1].children.length).toBe(2); //2 developer nodes under developer 4
    expect(tree.children[0].children[0].children[1].children[0].name).toBe(developerInput5.name); //developer 5 has developer 4 as parent
    expect(tree.children[0].children[0].children[1].children[1].name).toBe(developerInput6.name); //developer 6 has developer 4 as parent
    expect(tree.children[0].children[0].children[1].children[1].children.length).toBe(0); //developer 6 has no children
})

test("Get the tree structure from a starting node", async () => {
    //ARRANGE 
    const managerInput = {
        name: "manager with starting point",
        parentId: createdCeoId,
        nodeType: "manager",
        department: "IT",
    } satisfies CreateNodeCommand
    const createdManagerNode = await createNode(managerInput);

    const developerInput1 = {
        name: "Developer 1",
        parentId: createdManagerNode.id,
        nodeType: "developer",
        language: "JS",
    } satisfies CreateNodeCommand

    const developerInput2 = {
        name: "Developer 2",
        parentId: createdManagerNode.id,
        nodeType: "developer",
        language: "SCALA",
    } satisfies CreateNodeCommand

    const createdDev1 = await createNode(developerInput1);
    await createNode(developerInput2);

    const developerInput3 = {
        name: "Developer 3",
        parentId: createdDev1.id,
        nodeType: "developer",
        language: "JS",
    } satisfies CreateNodeCommand

    const developerInput4 = {
        name: "Developer 4",
        parentId: createdDev1.id,
        nodeType: "developer",
        language: "SCALA",
    } satisfies CreateNodeCommand

    await createNode(developerInput3);
    await createNode(developerInput4);
    //ACT

    const tree = await getTree(createdManagerNode.id);
    console.log("[Get the tree structure from a starting node test] - Tree: ", JSON.stringify(tree, null, 2));

    //ASSERT
    expect(tree.height).toBe(1); //We are starting the tree from the manager node
    expect(tree.children.length).toBe(2); //2 developer nodes under manager
    expect(tree.children[0].children.length).toBe(2); //2 developer nodes under developer 1
    expect(tree.children[0].children[0].children.length).toBe(0); //developer 3 has no children
    expect(tree.children[0].children[1].children.length).toBe(0); //developer 4 has no children
})