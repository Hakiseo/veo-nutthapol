import { afterAll, beforeAll, expect, test } from "vitest";
import { db } from "../src/dbConnection";
import { CreateNodeCommand, createNode } from "../src/mutations/createNode";

let createdCeoId: number;

//Running tests currently deletes all data in the database
beforeAll(async () => {
    await db.node.deleteMany();

    const createdCeo = await db.node.create({
        data: {
            name: "CEO",
            height: 0,
            nodeType: "ceo",
        }
    })
    createdCeoId = createdCeo.id;
})

afterAll(async () => {
    await db.node.deleteMany();
})

test("Should be able to create a node", async () => {
    //ARRANGE
    const nodeInput = {
        name: "Node 1",
        parentId: createdCeoId,
        nodeType: "developer",
        language: "JS",
    } satisfies CreateNodeCommand

    //ACT
    const createdNode = await createNode(nodeInput);

    //ASSERT
    expect(createdNode.name).toBe(nodeInput.name);
})