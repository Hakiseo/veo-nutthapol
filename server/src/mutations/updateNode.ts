import { z } from "zod";
import { db } from "../dbConnection";
import { getDescendantIds } from "../queries/getTree";

export type UpdateNodeCommand = z.input<typeof updateNodeParentValidator>

export async function updateParentOfNode(rawCommand: UpdateNodeCommand) {
    try {
        const { newParentId, nodeId } = updateNodeParentValidator.parse(rawCommand);

        if (newParentId === nodeId) {
            throw new Error("[updateParentOfNode] - Node cannot be its own parent")
        }

        const newParentHeight = await db.node.findUnique({
            where: { id: newParentId },
            select: {
                height: true
            }
        })

        if (newParentHeight === null) {
            throw new Error("[updateParentOfNode] - Parent not found")
        }

        const currentNode = await db.node.findUniqueOrThrow({
            where: { id: nodeId },
            select: {
                children: {
                    select: {
                        id: true,
                    }
                }
            }
        })

        const childrenOfCurrent = await getDescendantIds(currentNode.children.map(child => child.id))
        if (childrenOfCurrent.includes(newParentId)) {
            throw new Error("Parent cannot be a child of the node being moved")
        }

        const updatedNode = await db.node.update({
            where: { id: nodeId },
            data: {
                height: newParentHeight.height + 1,
                parentId: newParentId
            },
            include: {
                children: {
                    select: {
                        id: true,
                    }
                }
            }
        })

        const childrenToUpdate = updatedNode.children
            .map(child => {
                return { id: child.id, parentHeight: updatedNode.height }
            })

        await updateHeightOfChildren(childrenToUpdate)

        return updatedNode;
    } catch (error: unknown) {
        console.error(`[updateParentOfNode] - ${error}`);
        throw error;
    }
}

async function updateHeightOfChildren(nodestoUpdate: { id: number, parentHeight: number }[]) {
    nodestoUpdate.forEach(async child => {
        const updatedNode = await db.node.update({
            where: { id: child.id },
            data: {
                height: child.parentHeight + 1
            },
            select: {
                height: true,
                children: {
                    select: {
                        id: true,
                    }
                }
            }
        })

        if (updatedNode.children.length > 0) {
            const childrenToUpdate = updatedNode.children.map(child => { return { id: child.id, parentHeight: updatedNode.height } })
            await updateHeightOfChildren(childrenToUpdate)
        }
    })
}

export const updateNodeParentValidator = z.object({
    nodeId: z.number(),
    newParentId: z.number()
})