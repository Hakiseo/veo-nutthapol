import { z } from "zod";
import { db } from "../dbConnection";

export type CreateNodeCommand = z.input<typeof createNodeValidator>

export async function createNode(rawCommand: CreateNodeCommand) {
    try {
        const command = createNodeValidator.parse(rawCommand);

        if (command.nodeType === "ceo") {
            const ceo = await db.node.findFirst({
                where: {
                    nodeType: "ceo"
                }
            })

            if (ceo !== null) {
                throw new Error("There can only be one CEO!")
            }
        }

        let height = 0;
        if (command.parentId !== null) {
            const parent = await db.node.findUnique({
                where: { id: command.parentId },
                select: {
                    height: true
                }
            })

            if (parent !== null) {
                height = parent.height + 1;
            }
        }

        const createdNode = await db.node.create({
            data: {
                ...command,
                height
            }
        })

        return createdNode;
    } catch (error) {
        console.error(`[createNode] - ${error}`)
        throw error;
    }
}

const createNodeValidator = z.object({
    name: z.string(),
    nodeType: z.string()
        .transform(value => value.toLowerCase())
        .refine(value => ["ceo", "manager", "developer"]
            .some(type => type === value),
            "Invalid node type - must be: 'ceo', 'manager' or 'developer'"
        ),
    language: z.string().nullable().default(null),
    department: z.string().nullable().default(null),
    parentId: z.number().nullable().default(null),
}).refine(data => {
    if (data.nodeType.toLowerCase() === "ceo") {
        return data.parentId === null;
    }
    return data.parentId !== null;
}, "Only the CEO can have a null parentId")
    .refine(data => {
        if (data.nodeType.toLowerCase() === "developer") {
            return data.language !== null;
        }
        return true;
    }, "A developer must have a language")
    .refine(data => {
        if (data.nodeType.toLowerCase() === "manager") {
            return data.department !== null;
        }
        return true;
    }, "A manager must have a department")