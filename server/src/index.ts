import bodyParser from "body-parser";
import cors from "cors";
import { config } from "dotenv";
import express, { Request, Response } from "express";
import { createNode } from "./mutations/createNode";
import { getNodeWithChildren } from "./queries/getChildNodes";
import { getTree } from "./queries/getTree";
import { updateParentOfNode } from "./mutations/updateNode";

config()

const app = express();
const port = process.env.PORT || 3000;

app.use(cors());
app.use(bodyParser.json());

app.get('/', (req: Request, res: Response) => {
    res.json({ message: 'Hello, TypeScript Express!' });
});

app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}`);
});

app.post('/node', async (req: Request, res: Response) => {
    try {
        if (!req.body) {
            throw new Error("No body provided")
        }
        const result = await createNode(req.body);
        res.status(200).json({ result });
    } catch (error: unknown) {
        console.error(error);

        if (error instanceof Error) {
            res.status(400).json({ message: `Error adding node - ${error.message}` });
            return;
        }
        res.status(500).json({ message: 'Error adding node' });
    }
})

app.get('/node/:nodeId/childNodes', async (req: Request, res: Response) => {
    try {
        if (!req.params.nodeId) throw new Error('No node id provided');
        if (isNaN(Number(req.params.nodeId))) throw new Error('Node id must be a number');

        const nodeWithChildren = await getNodeWithChildren(Number(req.params.nodeId));
        res.status(200).json({ nodeWithChildren });
    } catch (error: unknown) {
        console.error(error);
        if (error instanceof Error) {
            res.status(400).json({ message: error.message });
            return;
        }
        res.status(500).json({ message: 'Error getting node with children' });
    }
})

app.get('/node/tree', async (req: Request, res: Response) => {
    try {
        const tree = await getTree();
        res.json({ tree });
    } catch (error: unknown) {
        console.error(error);
        if (error instanceof Error) {
            res.status(400).json({ message: error.message });
            return;
        }
        res.status(500).json({ message: 'Error getting tree' });
    }
})

app.patch('/node', async (req: Request, res: Response) => {
    try {
        if (!req.body) {
            throw new Error("No body provided")
        }
        const updatedNode = await updateParentOfNode(req.body);

        res.status(200).json({ updatedNode });
    } catch (error: unknown) {
        console.error(error);
        if (error instanceof Error) {
            res.status(400).json({ message: error.message });
            return;
        }
        res.status(500).json({ message: 'Error updating node' });
    }
})