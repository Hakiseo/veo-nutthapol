import { db } from "../dbConnection";

//Currently only getting 1 layer of children as required in the task
export async function getNodeWithChildren(nodeId: number) {
    try {
        const specificNodeWithChildren = await db.node.findUniqueOrThrow({
            where: { id: nodeId },
            include: {
                children: true
            }
        })

        return specificNodeWithChildren
    } catch (error: unknown) {
        console.error(`[getNodeWithChildren] - ${error}`);
        throw error;
    }
}