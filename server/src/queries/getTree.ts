import { db } from "../dbConnection";
import { Node as PrismaNode } from "@prisma/client"
import { getNodeWithChildren } from "./getChildNodes";

type NodeWithChildren = PrismaNode & {
    children: NodeWithChildren[];
}

//Assuming only 1 CEO node exists in the database we can just get the node with height 0
export async function getTree(startNodeId?: number) {
    try {
        const tree = await db.node.findFirstOrThrow({
            where: startNodeId ? { id: startNodeId } : { height: 0 },
            include: {
                children: {
                    select: {
                        id: true
                    }
                }
            }
        })

        const rootChildrenIds = tree.children.map(node => node.id)

        const descendants = await getDescendants(rootChildrenIds)
        const treeRootWithDescendants = { ...tree, children: descendants }
        //console.log("[getTree] - Tree with descendants: ", JSON.stringify(treeRootWithDescendants, null, 2))

        return treeRootWithDescendants
    } catch (error: unknown) {
        console.error(error);
        throw error;
    }
}

async function getDescendants(ids: number[]): Promise<NodeWithChildren[]> {
    const descendantsPromises = ids.map(async id => {
        const descendants = await getNodeWithChildren(id)
        return descendants
    })

    const resolvedDescendants = await Promise.all(descendantsPromises)
    //console.log("[getDescendants] - New children: ", resolvedDescendants)

    const childrenOfDescendants = resolvedDescendants.flatMap(node => node.children)
    //console.log("[getDescendants] - Children of newChildren: ", childrenOfDescendants)

    if (childrenOfDescendants.length === 0) {
        const descendants = resolvedDescendants.flatMap(node => {
            return { ...node, children: [] }
        })
        return descendants;
    } else {
        const descendants = await Promise.all(
            resolvedDescendants
                .flatMap(async node => {
                    const childrenIds = childrenOfDescendants
                        .filter(childNode => childNode.parentId === node.id)
                        .map(childNode => childNode.id)
                    const children = childrenIds.length > 0 ? await getDescendants(childrenIds) : []
                    return { ...node, children }
                }))

        return descendants;
    }
}

//Ugle copy of getDescendants, but this one only returns the ids
//TODO: Refactor to use getDescendants instead of this
export async function getDescendantIds(ids: number[]): Promise<number[]> {
    const descendantsPromises = ids.map(async id => {
        const descendants = await getNodeWithChildren(id)
        return descendants
    })

    const resolvedDescendants = await Promise.all(descendantsPromises)

    const childrenOfDescendants = resolvedDescendants.flatMap(node => node.children)

    if (childrenOfDescendants.length === 0) {
        const descendants = resolvedDescendants.flatMap(node => {
            return node.id
        })
        return descendants;
    } else {
        const descendants = await Promise.all(
            resolvedDescendants
                .flatMap(async node => {
                    const childrenIds = childrenOfDescendants
                        .filter(childNode => childNode.parentId === node.id)
                        .map(childNode => childNode.id)
                    const children = childrenIds.length > 0 ? await getDescendantIds(childrenIds) : []

                    const ids = [node.id].concat(children)
                    return ids
                }))

        return descendants.flat();
    }
}