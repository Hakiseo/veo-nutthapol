import { Button, Stack, TextField } from "@mui/material";
import { useState } from "react";
import { updateNodeParent } from "~/services/updateNode";

export function UpdateNodeForm(props: { onSuccess: () => Promise<void> }) {
    const { mutateAsync: updateNode, isSuccess, isError, error, isPending } = updateNodeParent()

    const [currentNodeId, setCurrentNodeId] = useState<string>('')
    const [newParentNodeId, setNewParentNodeId] = useState<string>('')

    function isStringNumber(value: string) {
        return !Number.isNaN(Number.parseInt(value))
    }

    async function handleUpdate() {
        if (!currentNodeId) {
            alert("currentNodeId is required!")
            return
        }

        if (!newParentNodeId) {
            alert("newParentNodeId is required!")
            return
        }

        if (!isStringNumber(currentNodeId)) {
            alert("currentNodeId should be a number!")
            return
        }

        if (!isStringNumber(newParentNodeId)) {
            alert("newParentNodeId should be a number!")
            return
        }

        try {
            await updateNode({
                nodeId: Number.parseInt(currentNodeId),
                newParentId: Number.parseInt(newParentNodeId)
            })
        } catch (error) {
            console.info("ERROR CUSTOM: ", error)
            return
        }

        setCurrentNodeId("")
        setNewParentNodeId("")
        props.onSuccess()
    }

    //TODO: ADD further error messages
    return (
        <div style={{ paddingRight: "3em", paddingLeft: "3em" }}>
            <h2>Update Node Form</h2>
            <Stack spacing={1}>
                <TextField value={currentNodeId} label="Current Node Id" onChange={e => setCurrentNodeId(e.target.value)}></TextField>
                <TextField value={newParentNodeId} label="New Parent Node Id" onChange={e => setNewParentNodeId(e.target.value)}></TextField>
                <Button variant="outlined" onClick={async () => await handleUpdate()}>Update Node with a new parentId!</Button>
                {isSuccess && <p style={{ color: "green" }}>Successfully updated node!</p>}
                {error && <p style={{ color: "red" }}>{error.message}</p>}
            </Stack>
        </div>
    )
}
