import { Button, Tooltip, TooltipProps, Typography, styled, tooltipClasses } from "@mui/material";
import { Tree, getTree } from "~/services/getTree";

export function SimpleTree() {
    const { data, isFetching: treeFetching, error: getTreeError, refetch: refetchTree } = getTree()

    return (
        <div style={{ paddingRight: "3em", paddingLeft: "3em" }}>
            <h2> Tree: </h2>
            {treeFetching && <p>loading ...</p>}
            {getTreeError && <p>{getTreeError.message}</p>}

            <div>
                {data && <p>{renderTree(data.tree)}</p>}
            </div>

            <Button variant="outlined" onClick={() => refetchTree()}> Refresh Tree </Button>
        </div>
    )
}

//Straight up copy from MUI docs
const HtmlTooltip = styled(({ className, ...props }: TooltipProps) => (
    <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
    [`& .${tooltipClasses.tooltip}`]: {
        backgroundColor: '#f5f5f9',
        color: 'rgba(0, 0, 0, 0.87)',
        maxWidth: 220,
        fontSize: theme.typography.pxToRem(12),
        border: '1px solid #dadde9',
    },
}));

function renderTree(tree: Tree) {
    return (
        <ul>
            <HtmlTooltip
                title={
                    <>
                        <Typography color="inherit">{tree.name}</Typography>
                        <p> id: {tree.id} </p>
                        <p> name: {tree.name}</p>
                        <p> role: {tree.nodeType} </p>
                        <p> parent: {tree.parentId} </p>
                        <p> language: {tree.language}</p>
                        <p> department: {tree.department}</p>
                    </>
                }
            >
                <li>Id: {tree.id} - Name: {tree.name} - Role: {tree.nodeType}</li>
            </HtmlTooltip>
            {tree.children.map((child) => renderTree(child))}
        </ul>
    )
}