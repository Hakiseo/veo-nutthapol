import { Button, InputLabel, MenuItem, Select, Stack, TextField } from "@mui/material"
import { useState } from "react"
import { createNode } from "~/services/createNode"

export function NewNodeForm(props: { onSuccess: () => Promise<void> }) {
    const {
        mutateAsync: createNewNode,
        error: createNewNodeError,
        isPending: creatingNewNode,
        isSuccess: newNodeCreated,
    } = createNode()

    const [customError, setCustomError] = useState("")

    const [name, setName] = useState<string>("")
    const [nodeType, setNodeType] = useState<string>("")
    const [language, setLanguage] = useState<string>("")
    const [parentId, setParentId] = useState<string>("")
    const [department, setDepartment] = useState<string>("")

    function resetForm() {
        setName("")
        setNodeType("")
        setLanguage("")
        setParentId("")
        setDepartment("")
        setCustomError("")
    }

    //Assuming that the user will always enter a valid parentId
    async function handleSubmit() {
        setCustomError("")
        if (!name) {
            setCustomError("name is required!")
            return
        }

        if (!nodeType) {
            setCustomError("Role is required!")
            return
        }

        if (isLanguageRequired() && !language) {
            setCustomError("language is required for developers!")
            return
        }

        if (isDepartmentRequired() && !department) {
            setCustomError("department is required for managers!")
            return
        }

        if (isParentRequired()) {
            if (!parentId) {
                setCustomError("parentId is required!")
                return
            }

            if (!isStringANumber(parentId)) {
                setCustomError("parentId should be a number!")
                return
            }

            await createNewNode({
                name,
                nodeType,
                language,
                department,
                parentId: Number.parseInt(parentId),
            })
        } else {
            await createNewNode({
                name,
                nodeType,
                language: isLanguageRequired() ? language : undefined,
                department: isDepartmentRequired() ? department : undefined,
            })
        }

        resetForm()
        props.onSuccess()
    }

    function isStringANumber(value: string) {
        return !Number.isNaN(Number.parseInt(value))
    }

    function isParentRequired() {
        return nodeType !== "ceo"
    }

    function isLanguageRequired() {
        return nodeType === "developer"
    }

    function isDepartmentRequired() {
        return nodeType === "manager"
    }

    //TODO: LOTS OF STYLING
    //TODO: parentId textfield should probably be a select showing names of all existing node while value is id of the node
    return (
        <div style={{ paddingRight: "3em", paddingLeft: "3em" }}>
            <h2>Create New Node</h2>
            <Stack spacing={1} >
                <TextField value={name} onChange={e => setName(e.target.value)} label="name"></TextField>
                <InputLabel id="nodeTypeSelectInput">Role</InputLabel>
                <Select
                    value={nodeType}
                    labelId="nodeTypeSelectInput"
                    label="Node Type"
                    onChange={e => setNodeType(e.target.value)}
                >
                    <MenuItem value={""}>None</MenuItem>
                    <MenuItem value={"developer"}>Developer</MenuItem>
                    <MenuItem value={"manager"}>Manager</MenuItem>
                    <MenuItem value={"ceo"}>CEO</MenuItem>
                </Select>

                <InputLabel id="departmentSelectInput">Department</InputLabel>
                <Select
                    value={department}
                    labelId="departmentSelectInput"
                    label="Department"
                    onChange={e => setDepartment(e.target.value)}
                >
                    <MenuItem value={""}>None</MenuItem>
                    <MenuItem value={"development"}>Development</MenuItem>
                    <MenuItem value={"sales"}>Sales</MenuItem>
                    <MenuItem value={"support"}>Support</MenuItem>
                </Select>

                <TextField value={language} onChange={e => setLanguage(e.target.value)} label="language"></TextField>

                <TextField value={parentId} onChange={e => setParentId(e.target.value)} label="parentId"></TextField>

                <Button variant="outlined" onClick={async () => await handleSubmit()}> Create new Node </Button>
            </Stack>

            {creatingNewNode && <p>Creating new node...</p>}
            {newNodeCreated && !customError && <p>Node created successfully!</p>}
            {customError && <p style={{ color: "red" }}>{customError}</p>}
        </div>
    )
}