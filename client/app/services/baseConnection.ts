import axios from "axios";

//The base url should correspond to the express-server url
export const api = axios.create({
    baseURL: "http://localhost:3000/",
})