import { useMutation } from "@tanstack/react-query"
import { api } from "./baseConnection"

export type UpdateNodeResponse = {
    updatedNode: {
        id: number;
        name: string;
        height: number;
        parentId: number | null;
        nodeType: string;
        department: string | null;
        language: string | null;
        createdAt: Date;
        updatedAt: Date;
        children: {
            id: number;
        }[];
    }
}
//TODO: extract proper error messages to display to the user
export function updateNodeParent() {
    const endpoint = "/node"

    return useMutation({
        mutationKey: ['updateNode'], mutationFn: async (command: { nodeId: number, newParentId: number }) => {
            const updatedNodeResponse = await api.patch<UpdateNodeResponse>(endpoint, command)
            if (updatedNodeResponse.status !== 200) throw new Error(updatedNodeResponse.statusText)

            return updatedNodeResponse.data
        }
    })
}