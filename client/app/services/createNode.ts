import { useMutation } from "@tanstack/react-query"
import { api } from "./baseConnection"

export type CreateNodeResponse = {
    result: {
        id: number;
        name: string;
        height: number;
        parentId: number | null;
        nodeType: string;
        department: string | null;
        language: string | null;
        createdAt: Date;
        updatedAt: Date;
    }
}

export type CreateNodeCommand = {
    name: string;
    nodeType: string;
    parentId?: number;
    department?: string;
    language?: string;
}

//TODO: extract proper error messages to display to the user
export function createNode() {
    const endpoint = "/node"

    return useMutation({
        mutationKey: ['createNode'], mutationFn: async (command: CreateNodeCommand) => {
            const createNodeResponse = await api.post<CreateNodeResponse>(endpoint, command)
            if (createNodeResponse.status !== 200) throw new Error(createNodeResponse.statusText)

            return createNodeResponse.data
        }
    })
}