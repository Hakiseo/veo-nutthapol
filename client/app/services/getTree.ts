import { useQuery } from "@tanstack/react-query"
import { api } from "./baseConnection"

export type Tree = {
    id: number,
    name: string,
    height: number,
    parentId: number | null,
    nodeType: string,
    department: string | null,
    language: string | null,
    createdAt: string,
    updatedAt: string,
    children: Tree[]
}

export type GetTreeResponse = {
    tree: Tree
}

//TODO: Invalidate query - https://react-query.tanstack.com/guides/query-invalidation
//If we have created test data manually with the web interface and then run the tests, the tree will not be updated properly
//TODO: extract proper error messages to display to the user
export function getTree() {
    const endpoint = '/node/tree'

    return useQuery({
        queryKey: ['getTree'], queryFn: async () => {
            const getTreeResponse = await api.get<GetTreeResponse>(endpoint)

            if (getTreeResponse.status !== 200) throw new Error(getTreeResponse.statusText)

            return getTreeResponse.data
        }
    })
}