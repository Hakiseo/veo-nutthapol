import type { LinkDescriptor, MetaFunction } from "@remix-run/node";
import { NewNodeForm } from "~/components/createNodeForm";
import { SimpleTree } from "~/components/treeStructure";
import { UpdateNodeForm } from "~/components/updateNodeForm";

import globalStyles from "../global.css"
import styled from "@emotion/styled";
import { Button, TooltipProps, Tooltip, tooltipClasses, Typography } from "@mui/material";
import { getTree, Tree } from "~/services/getTree";

export function links(): LinkDescriptor[] {
  return [
    {
      rel: "stylesheet",
      href: globalStyles
    }
  ]
}

export const meta: MetaFunction = () => {
  return [
    { title: "New Remix App" },
    { name: "description", content: "Welcome to Remix!" },
  ];
};

export default function Index() {
  const { data, isFetching: treeFetching, error: getTreeError, refetch: refetchTree } = getTree()

  async function handleOnSuccess() {
    await refetchTree()
  }

  return (
    <div className="gridContainer">
      <div>
        <NewNodeForm onSuccess={() => handleOnSuccess()} />
        <UpdateNodeForm onSuccess={() => handleOnSuccess()} />
      </div>

      <div style={{ paddingRight: "3em", paddingLeft: "3em" }}>
        <h2> Tree: </h2>
        {treeFetching && <p>loading ...</p>}
        {getTreeError && <p>{getTreeError.message}</p>}

        <div>
          {data && <p>{renderTree(data.tree)}</p>}
        </div>

        <Button variant="outlined" onClick={() => refetchTree()}> Refresh Tree </Button>
      </div>
    </div>
  );
}

//Straight up copy from MUI docs
const HtmlTooltip = styled(({ className, ...props }: TooltipProps) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: '#f5f5f9',
    color: 'rgba(0, 0, 0, 0.87)',
    maxWidth: 220,
    border: '1px solid #dadde9',
  },
}));

function renderTree(tree: Tree) {
  return (
    <ul>
      <HtmlTooltip
        title={
          <>
            <Typography color="inherit">{tree.name}</Typography>
            <p> id: {tree.id} </p>
            <p> name: {tree.name}</p>
            <p> role: {tree.nodeType} </p>
            <p> parent: {tree.parentId} </p>
            <p> language: {tree.language}</p>
            <p> department: {tree.department}</p>
          </>
        }
      >
        <li>Id: {tree.id} - Name: {tree.name} - Role: {tree.nodeType}</li>
      </HtmlTooltip>
      {tree.children.map((child) => renderTree(child))}
    </ul>
  )
}
